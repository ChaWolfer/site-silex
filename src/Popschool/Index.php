<?php

namespace Popschool;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class Index
{
    public function action(Request $request, Application $app)
    {
	    return $app['twig']->render('index.html.twig', [
	    ]);
	}
}
