<?php

require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();

// activation du mode de déboggage (en phase de dev)
// à désactiver en prod
$app['debug'] = true;

// chargement du fichier de config
$app->register(
        new GeckoPackages\Silex\Services\Config\ConfigServiceProvider(),
        array(
            'config.dir' => __DIR__.'/../app/config',
            'config.format' => '%key%.%env%.yml',
            'config.env' => 'dev',
        )
);

// connexion à la base de données
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver'    => 'pdo_mysql',
        'host'      => $app['config']['database']['host'],
        'dbname'    => $app['config']['database']['dbname'],
        'user'      => $app['config']['database']['user'],
        'password'  => $app['config']['database']['password'],
        'charset'   => $app['config']['database']['charset'],
   	),
));

// chargement du moteur de template
$app->register(new Silex\Provider\TwigServiceProvider(), [
    'twig.path' => __DIR__.'/../app/Ressources/views',
]);

include(__DIR__ . '/../app/routes.php');

$app->run();
